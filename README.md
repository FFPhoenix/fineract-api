
# How to:

## Requirements

```
node -v
v8.11.1
```

```
npm -v 
6.3.0
```

## Installation Steps

Clone app
```
git clone https://FFPhoenix@bitbucket.org/FFPhoenix/fineract-api.git
```

Init app 
```
npm i
```


## Code Documentation
Found in 
```
 app_dir/docs/index.html
```
