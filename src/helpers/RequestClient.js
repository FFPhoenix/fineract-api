const provider = require('request-promise');
const errorHandler = require('./ErrorHandler')

/**
 * request wrapper
 * @type {RequestClient}
 */
const RequestClient = class RequestClient {

    /**
     * @ignore
     */
    constructor() {

        /**
         * @type {ErrorHandler} Error handling class
         */
        this.errorHandler = errorHandler;

        /**
         * @type {request} request provider
         */
        this.requestProvider = provider;

        /**
         * @access {private}
         * @type {string}
         */
        this.baseUrl = `https://demo.openmf.org/fineract-provider/api/`;

        /**
         * @access {private}
         * @type {string}
         */
        this.apiVersion = `v1/`;

        /**
         * @type {Object} request options object
         */
        this.options = {
            method: 'GET',
            body: {},
            qs: {},
            resolveWithFullResponse: true,
            simple: true,
            json: true,
            headers: {
                'Content-Type': 'application/json',
                'Fineract-Platform-TenantId': 'default',
            },
        };
    }

    /**
     * @return {string}
     */
    get getRequestUrl() {
        return this.baseUrl + this.apiVersion;
    }

    /**
     * @param {Object} body
     */
    set body(body) {
        this.options.body = body;
    }

    /**
     * @param {Object} query
     */
    set query(query) {
        this.options.qs = query;
    }

    /**
     *
     * @return {Object}
     */
    get requestHeaders() {
        return this.options.headers;
    }

    /**
     *
     * @param {Objet} headers
     */
    set requestHeaders(headers) {
        this.options.headers = headers;
    }

    /**
     *
     * @param {string} resourcePath
     * @param {string} method
     * @return {Promise<void>}
     */
    async execRequest(resourcePath, method) {
        try {
            this.options.uri = this.getRequestUrl + resourcePath;
            this.options.method = method;
            const requestResult = await this.requestProvider(this.options);

            return requestResult.body;
        } catch (error) {
            throw this.errorHandler.handle(error);
        }
    }

    /**
     *
     * @param {string} resourcePath
     * @param {Object} query
     * @return {Promise<void>}
     */
    get(resourcePath, query = {}) {
        this.query = query;

        return this.execRequest(resourcePath, 'GET');
    }

    /**
     *
     * @param {string} resourcePath
     * @param {Object} body
     * @param {Object} query
     * @return {Promise<void>}
     */
    post(resourcePath, body = {}, query = {}) {
        this.body = body;
        this.query = query;

        return this.execRequest(resourcePath, 'POST');
    }

    /**
     *
     * @param {string} resourcePath
     * @param {Object} body
     * @return {Promise<void>}
     */
    put(resourcePath, body = {}) {
        this.body = body;

        return this.execRequest(resourcePath, 'PUT');
    }

    /**
     *
     * @param {string} resourcePath
     * @param {Object} body
     * @param {Object} query
     * @return {Promise<void>}
     */
    delete(resourcePath, body = {}, query = {}) {
        this.body = body;
        this.query = query;

        return this.execRequest(resourcePath, 'DELETE');
    }

};

module.exports = RequestClient;