const Config = require('../config');

/**
 * auth helper class
 * @type {Auth}
 */
const Auth = class Auth {

    /**
     * @param {RequestClient} request - Link to Request Client
     */
    constructor(request) {

        /**
         * @type {Object} request
         * @access {private}
         */
        this.request = request;

        /**
         * @type {string} request
         */
        this.authToken = null;
    }


    /**
     * auth user in 3d party system
     * @param {String} username
     * @param {String} password
     * @return {JSON}
     */
    login(username = null, password = null) {
        let query = {};
        if (username === null && password === null) {
            query = {
                username: Config.username,
                password: Config.password,
            };
        } else {
            query = {
                username,
                password,
            };
        }

        return this.request.post('authentication', {}, query)
            .then((response) => {
                this.authToken = response.base64EncodedAuthenticationKey;
                const headers = this.request.requestHeaders;
                headers.Authorization = `Basic ${this.authToken}`;
                this.request.requestHeaders = headers;

                return response;
            })
            .catch((reason) => reason);
    }
};

module.exports = Auth;
