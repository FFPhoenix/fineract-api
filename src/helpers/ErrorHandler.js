const httpStatus = require('http-status');

/**
 * discribe errors what happend in Request
 * @type {RequestError}
 */
class RequestError extends Error {

    /**
     * @param {string} message
     * @param {Object} body
     * @param {number} status
     */
    constructor(message, body, status) {
        super(message);

        /**
         * @type {Object}
         */
        this.body = body;

        /**
         * @type {number|500}
         */
        this.status = status || httpStatus.INTERNAL_SERVER_ERROR;
    }
}

/**
 * discribe api errors
 * @type {ServiceError}
 */
class ServiceError extends Error {

    /**
     * @param {string} message
     * @param {number} status
     */
    constructor(message, status) {
        super(message);

        /**
         * @type {number|500}
         */
        this.status = status || httpStatus.INTERNAL_SERVER_ERROR;
    }
}

/**
 * error Handler
 * @todo improve errors handling
 * @type {ErrorHandler}
 */
const ErrorHandler = class ErrorHandler {

    /**
     * handle request error
     * @params {Object} error
     * @return {RequestError|ServiceError}
     */
    static handle(error) {

        switch (error.statusCode) {
            case 400:
            case 401:
            case 403:
            case 404:
            case 409:
                const errorBody = error.response && error.response.body ? error.response.body : '';
                return new RequestError(error.message, errorBody, error.statusCode);
            default:
                return new ServiceError(error.message);

        }
    }
};


module.exports = ErrorHandler;
