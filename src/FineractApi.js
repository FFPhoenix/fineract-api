const RequestClient = require('./helpers/RequestClient');
const Auth = require('./helpers/Auth');

const Client = require('./module/Client');
const Loan = require('./module/Loan');
const LoanProduct = require('./module/LoanProduct');

/**
 * main api wrapper class
 */
const FineractApi = class FineractApi {

    /**
     * @ignore
     */
    constructor() {

        /**
         * @type {RequestClient}
         * @access {private}
         */
        this.request = new RequestClient();

        /**
         * @type {Auth}
         * @access {private}
         */
        this.auth = new Auth(this.request);

        /**
         * @type {Client} Client api calls
         */
        this.client = new Client(this.request);

        /**
         * @type {Loan} Loan api calls
         */
        this.loan = new Loan(this.request);

        /**
         * @type {LoanProduct} LoanProduct api calls
         */
        this.loanProduct = new LoanProduct(this.request);
    }


    /**
     * call Auth helper
     * @example
     * const callApi = new FineractApi();
     * callApi.login('username', 'password');
     *
     * @param {sting} username
     * @param {sting} password
     * @return {Promice}
     */
    async login(username = null, password = null) {
        const result = await this.auth.login(username, password);
        return result;
    }
}

module.exports = FineractApi;