const Base = require('./Base');

/**
 * @type {string}
 */
const COMMAND_APPROVE = 'approve';

/**
 * @type {string}
 */
const COMMAND_REJECT = 'reject';

/**
 * @type {string}
 */
const COMMAND_WITHDRAW = 'withdrawnByApplicant';

/**
 * loan application api calls
 * @type {Loan}
 */
const Loan = class Loan extends Base {

    /**
     * @param {RequestClient} request
     */
    constructor(request) {
        super(request);

        /**
         * @type {string}
         */
        this.resourcePath = 'loans';
    }

    /**
     *
     * @param {Object} body
     * @return {Promise<void>}
     */
    create(body) {
        return super.create(body);
    }

    /**
     * @param {number} id
     * @param {string} command
     * @return {Provice}
     */
    execCommand(id, command) {
        if ([COMMAND_APPROVE,
            COMMAND_REJECT,
            COMMAND_WITHDRAW].indexOf(command) === -1) {
            throw Error('Command not found');
        }

        return this.request.post(`${this.resourcePath}/${id}`, {}, { command });
    }

    /**
     * @param {number} id
     * @return {Provice}
     */
    approve(id) {
        return this.execCommand(id, COMMAND_APPROVE);
    }


    /**
     * @param {number} id
     * @return {Provice}
     */
    reject(id) {
        return this.execCommand(id, COMMAND_REJECT);
    }


    /**
     * @param {number} id
     * @return {Provice}
     */
    withdraw(id) {
        return this.execCommand(id, COMMAND_WITHDRAW);
    }

}

module.exports = Loan;