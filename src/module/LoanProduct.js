const Base = require('./Base');

/**
 * loanProduct api calls
 * @type {LoanProduct}
 */
const LoanProduct = class LoanProduct extends Base {

    /**
     * @param {RequestClient} request
     */
    constructor(request) {
        super(request);

        /**
         * @type {string}
         */
        this.resourcePath = 'loanproducts';
    }


    /**
     *
     * @param {Object} body
     * @return {Promise<void>}
     */
    create(body) {
        return super.create(body);
    }
}

module.exports = LoanProduct;