const Base = require('./Base');

/**
 * client api calls
 * @type {Client}
 */
const Client = class Client extends Base {

    /**
     * @param {RequestClient} request
     */
    constructor(request) {
        super(request);

        /**
         * @type {string}
         */
        this.resourcePath = 'clients';
    }

    /**
     * create a new client
     * @param {Object}  body
     * @param {Strings} body.firstname
     * @param {Strings} body.lastname
     * @param {Strings} body.fullname
     * @param {Number}  body.officeId
     * @param {Boolean} body.active
     * @param {Boolean} body.activationDate
     * @param {Array}   body.address
     * @return {Promise}
     */
    create(body) {
        return super.create(body);
    }
}

module.exports = Client;