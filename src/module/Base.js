/**
 * abstract Base module class
 * @type {Base}
 */
const Base = class Base {

    /**
     *
     * @param {RequestClient} request
     */
    constructor(request) {

        /**
         * @type {RequestClient}
         */
        this.request = request;

        /**
         * @type {string}
         */
        this.resourcePath = '';
    }

    /**
     * get one resource by resourcePath
     * @param {number} id
     * @return {Promise<void>}
     */
    retrieveOne(id) {
        return this.request.get(`${this.resourcePath}/${id}`);
    }

    /**
     * get list of resources by resourcePath
     * @return {Promise<void>}
     */
    retrieveAll() {
        return this.request.get(this.resourcePath);
    }

    /**
     * create resource with body
     * @param {Object} body
     * @return {Promise<void>}
     */
    create(body) {
        return this.request.post(this.resourcePath, body);
    }
}

module.exports = Base;