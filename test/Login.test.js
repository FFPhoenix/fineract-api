const expect = require('chai').expect;
const nock = require('nock');
const mockData = require('./mockData')

const Auth = require('../src/helpers/Auth');
const Request = require('../src/helpers/RequestClient');

const authApi = new Auth(new Request());

describe('Login API test', () => {
    beforeEach(() => {
    });

    it('Check correct Login', (done) => {
        nock(mockData.url)
            .post(/.*/).once()
            .reply(200, mockData.res.login);

        authApi.login()
            .then((response) => {
                expect(response.base64EncodedAuthenticationKey)
                    .to.equal(mockData.res.login.base64EncodedAuthenticationKey);
            }).then(done, done);

    });

    it('Check uncorrect Login', (done) => {
        nock(mockData.url)
            .post(/.*/).once()
            .reply(401, mockData.res.badLogin);

        authApi.login()
            .then((response) => {
                expect(response).to.have.own.property('status');
                expect(response.status)
                    .to.equal(mockData.res.badLogin.httpStatusCode);
            }).then(done, done);

    });
});