const Faker = require('faker');

const today = new Date().toLocaleDateString('en-GB');
module.exports = {
    newClient : {
        activationDate: today,
        active : false,
        address : [],
        dateFormat: 'm/dd/yyyy',
        firstname :Faker.name.firstName(),
        lastname : Faker.name.lastName(),
        legalFormId : null,
        locale : "en",
        officeId : 104,
        savingsProductId : null,
        submittedOnDate : today,
    },
    badNewClient : {
        activationDate: today,
        active :false,
        address : [],
        dateFormat: 'm/dd/yyyy',
        firstname :Faker.name.firstName(),
        lastname : Faker.name.lastName(),
        legalFormId : null,
        locale : "en",
        savingsProductId : null,
        submittedOnDate : today,
    },
    res: {
        clientCreated : { officeId: 104, clientId: 5868, resourceId: 5868 },
        clientCteateError : {
            developerMessage:'The request was invalid. This typically will happen due to validation errors which are provided.',
            httpStatusCode:'400',
            defaultUserMessage:'Validation errors exist.',
            userMessageGlobalisationCode:'validation.msg.validation.errors.exist',
            errors:[
                {
                    developerMessage:'The parameter officeId is mandatory.',
                    defaultUserMessage:'The parameter officeId is mandatory.',
                    userMessageGlobalisationCode:'validation.msg.client.officeId.cannot.be.blank',
                    parameterName:'officeId',
                    value:null,
                },
            ],
        },
        clientGet : {
            id:5870,
            accountNo:'Morogoro b000005870',
            status:{
                id:100,
                code:'clientStatusType.pending',
                value:'Pending',
            },
            subStatus:{
                active:false,
                mandatory:false,
            },
            active:false,
            firstname:'Aurelia',
            lastname:'Lindgren',
            displayName:'Aurelia Lindgren',
            gender:{
                active:false,
                mandatory:false,
            },
            clientType:{
                active:false,
                mandatory:false,
            },
            clientClassification:{
                active:false,
                mandatory:false,
            },
            isStaff:false,
            officeId:104,
            officeName:'Morogoro branch',
            timeline:{
                submittedOnDate:[
                    2018,
                    1,
                    30,
                ],
                submittedByUsername:'mifos',
                submittedByFirstname:'App',
                submittedByLastname:'Administrator',
            },
            groups:[

            ],
            clientNonPersonDetails:{
                constitution:{
                    active:false,
                    mandatory:false,
                },
                mainBusinessLine:{
                    active:false,
                    mandatory:false,
                },
            },
        },

    },
};