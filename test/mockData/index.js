const timeNow = new Date();
module.exports = {
    url: `https://demo.openmf.org`,
    uri: `/fineract-provider/api/v1/`,
    res: {
        login: {
            username: 'mifos',
            userId: 1,
            base64EncodedAuthenticationKey: '9999999999999=',
            authenticated: true,
            officeId: 1,
            officeName: 'Head Office',
            roles:
                [{
                    id: 1,
                    name: 'Super user',
                    description: 'This role provides all application permissions.',
                    disabled: false,
                }],
            permissions: ['READ_GROUP',
                'ALL_FUNCTIONS',
                'CREATE_GROUP'],
            shouldRenewPassword: false,
            isTwoFactorAuthenticationRequired: false,
        },
        badLogin : {
            developerMessage: 'Invalid authentication details were passed in api request.',
            httpStatusCode: 401,
            defaultUserMessage: 'Unauthenticated. Please login.',
            userMessageGlobalisationCode: 'error.msg.not.authenticated',
            errors: [],
        },
        unauthorized : {
            timestamp: timeNow.getTime(),
            status: 401,
            error: 'Unauthorized',
            message: 'Full authentication is required to access this resource',
        },

    },
};