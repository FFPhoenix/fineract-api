const expect = require('chai').expect;
const nock = require('nock');
const mockData = require('./mockData')
const mockLoanProduct = require('./mockData/LoanProduct')

const Request = require('../src/helpers/RequestClient');
const LoanProduct = require('../src/module/LoanProduct');

const LoanProductApi = new LoanProduct(new Request());

describe('Loan Product API test', () => {
    beforeEach(() => {
    });

    it('Check retrieveOne()', (done) => {
        nock(mockData.url)
            .get(/.*/).once()
            .reply(200, mockLoanProduct.res.loanProductGet);

        LoanProductApi.retrieveOne(923)
            .then((response) => {
                expect(response).to.have.own.property('id');
                expect(response.id).to.equal(mockLoanProduct.res.loanProductGet.id);
            })
            .then(done, done);
    });

    it('Check positive create()', (done) => {
        nock(mockData.url)
            .post(/.*/).once()
            .reply(200, mockLoanProduct.res.loanProductCreated);

        LoanProductApi.create(mockLoanProduct.newLoanProduct)
            .then((response) => {
                expect(response).to.have.own.property('resourceId');
                expect(response.resourceId).to.equal(mockLoanProduct.res.loanProductCreated.resourceId);
            })
            .then(done, done);
    });

    it('Check negative create()', (done) => {
        nock(mockData.url)
            .post(/.*/).once()
            .reply(401, mockLoanProduct.res.loanProductCteateError);

        LoanProductApi.create(mockLoanProduct.badNewLoanProduct)
            .catch((error) => {
                expect(error.status).to.equal(401);
                expect(error.body.userMessageGlobalisationCode).to.equal('validation.msg.validation.errors.exist');
            }).then(done, done);
    });

});