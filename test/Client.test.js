const expect = require('chai').expect;
const nock = require('nock');
const mockData = require('./mockData')
const mockClient = require('./mockData/Client')

const Request = require('../src/helpers/RequestClient');
const Client = require('../src/module/Client');

const ClientApi = new Client(new Request());

describe('Client API test', () => {
    beforeEach(() => {
    });

    it('Check Unauthorized', (done) => {
        nock(mockData.url)
            .get(/.*/).once()
            .reply(401, mockData.res.unauthorized);

        ClientApi.retrieveAll()
            .catch((error) => {
                expect(error.status).to.equal(401);
            }).then(done, done);

    });

    it('Check retrieveOne()', (done) => {
        nock(mockData.url)
            .get(/.*/).once()
            .reply(200, mockClient.res.clientGet);

        ClientApi.retrieveOne(mockClient.res.clientGet.id)
            .then((response) => {
                expect(response).to.have.own.property('id');
                expect(response.id).to.equal(mockClient.res.clientGet.id);
            })
            .then(done, done);
    });

    it('Check positive create()', (done) => {
        nock(mockData.url)
            .post(/.*/).once()
            .reply(200, mockClient.res.clientCreated);

        ClientApi.create(mockClient.newClient)
            .then((response) => {
                expect(response).to.have.own.property('clientId');
                expect(response.clientId).to.equal(mockClient.res.clientCreated.clientId);
            })
            .then(done, done);
    });

    it('Check negative create()', (done) => {
        nock(mockData.url)
            .post(/.*/).once()
            .reply(401, mockClient.res.clientCteateError);

        ClientApi.create(mockClient.badNewClient)
            .catch((error) => {
                expect(error.status).to.equal(401);
                expect(error.body.userMessageGlobalisationCode).to.equal('validation.msg.validation.errors.exist');
            }).then(done, done);
    });

});