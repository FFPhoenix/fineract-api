const expect = require('chai').expect;
const nock = require('nock');
const mockData = require('./mockData')
const mockLoan = require('./mockData/Loan')

const Request = require('../src/helpers/RequestClient');
const Loan = require('../src/module/Loan');

const LoanApi = new Loan(new Request());

describe('Loan API test', () => {
    beforeEach(() => {
    });

    it('Check retrieveOne()', (done) => {
        nock(mockData.url)
            .get(/.*/).once()
            .reply(200, mockLoan.res.LoanGet);

        LoanApi.retrieveOne(923)
            .then((response) => {
                expect(response).to.have.own.property('id');
                expect(response.id).to.equal(mockLoan.res.LoanGet.id);
            })
            .then(done, done);
    });

    it('Check positive create()', (done) => {
        nock(mockData.url)
            .post(/.*/).once()
            .reply(200, mockLoan.res.LoanCreated);

        LoanApi.create(mockLoan.newLoan)
            .then((response) => {
                expect(response).to.have.own.property('resourceId');
                expect(response.resourceId).to.equal(mockLoan.res.LoanCreated.resourceId);
            })
            .then(done, done);
    });

    it('Check negative create()', (done) => {
        nock(mockData.url)
            .post(/.*/).once()
            .reply(401, mockLoan.res.LoanCteateError);

        LoanApi.create(mockLoan.badNewLoan)
            .catch((error) => {
                expect(error.status).to.equal(401);
                expect(error.body.userMessageGlobalisationCode).to.equal('validation.msg.validation.errors.exist');
            }).then(done, done);
    });

    it('Check approve()', (done) => {
        nock(mockData.url)
            .post(/.*/).once()
            .reply(200, mockLoan.res.LoanApproved);

        LoanApi.create(mockLoan.approve)
            .then((response) => {
                expect(response).to.have.own.property('loanId');
                expect(response.loanId).to.equal(mockLoan.res.LoanApproved.loanId);
            })
            .then(done, done);
    });

    it('Check withdraw()', (done) => {
        nock(mockData.url)
            .post(/.*/).once()
            .reply(200, mockLoan.res.LoanWithdrawn);

        LoanApi.create(mockLoan.withdrawn)
            .then((response) => {
                expect(response).to.have.own.property('loanId');
                expect(response.loanId).to.equal(mockLoan.res.LoanWithdrawn.loanId);
            })
            .then(done, done);
    });

    it('Check reject()', (done) => {
        nock(mockData.url)
            .post(/.*/).once()
            .reply(200, mockLoan.res.LoanRejected);

        LoanApi.create(mockLoan.reject)
            .then((response) => {
                expect(response).to.have.own.property('loanId');
                expect(response.loanId).to.equal(mockLoan.res.LoanRejected.loanId);
            })
            .then(done, done);
    });

});