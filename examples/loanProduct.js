const FineractApi = require('../index');
const fakeData = require('../test/mockData/LoanProduct');

const apiCall = new FineractApi();

apiCall.login().then(async () => {

    await apiCall.loanProduct.retrieveAll().then((clients) => {
        console.log(clients);
    });
    await apiCall.loanProduct.create(fakeData.badNewLoanProduct)
        .catch((response) => {
            console.log(response.status, response.body);
        });
    await apiCall.loanProduct.create(fakeData.newLoanProduct).then((response) => {
        console.log(response);
        apiCall.loanProduct.retrieveOne(response.resourceId).then((loanProducts) => {
            console.log(loanProducts);
        });
    });
});
