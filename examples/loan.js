const FineractApi = require('../index');
const fakeData = require('../test/mockData/Loan');

const apiCall = new FineractApi();
apiCall.login().then(async () => {


    await apiCall.loan.retrieveAll()
        .then((loans) => {
            console.log(loans);
        });
    await apiCall.loan.create(fakeData.badNewLoanProduct)
        .catch((response) => {
            console.log(response.status, response.body);
        });
    await apiCall.loan.create(fakeData.newLoan).then(async (response) => {
        console.log(response);

        await apiCall.loan.retrieveOne(response.resourceId).then((loan) => {
            console.log(loan);
        });

        await apiCall.loan.approve(response.resourceId).then(result => {
            console.log(result);
        });

        await apiCall.loan.withdraw(response.resourceId).then(result => {
            console.log(result);
        });

        await apiCall.loan.reject(response.resourceId).then(result => {
            console.log(result);
        });
    }).catch((error) => {
        console.log(error.status, error.body.errors);
    });
});
