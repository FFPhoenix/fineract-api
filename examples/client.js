const FineractApi = require('../index');
const fakeData = require('../test/mockData/Client');

const apiCall = new FineractApi();

apiCall.login().then(async () => {


    await apiCall.client.retrieveAll().then((clients) => {
        console.log(clients);
    });
    await apiCall.client.create(fakeData.badNewClient)
        .catch((response) => {
            console.log(response.status, response.body);
        });
    await apiCall.client.create(fakeData.newClient).then((response) => {
        console.log(response);
        apiCall.client.retrieveOne(response.clientId).then((client) => {
            console.log(client);
        });
    });
});
